Pod::Spec.new do |s|

  s.name                = "GeoTracking"
  s.version             = "1.0."
  s.summary             = "A library to fetch user location."
  s.description         = "A simple application to fetch user location."
  s.homepage            = "http://google.com"
  s.license             = "MIT"
  s.author              = "Bimal Sharma"
  s.platform            = :ios, "12.0"
  s.source              = { :git => "https://bimal_mobile_dev@bitbucket.org/bimal_mobile_dev/geotracking.git", :tag => "1.0.0" }
  s.source_files        = "GeoTracking/**/*.{h,m,swift}"
  s.swift_version       = "4.2"
end