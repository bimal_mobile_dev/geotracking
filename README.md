# GeoTracking
A simple swift library to get user's location.

Requirements

iOS 11.0+
Swift 4.0+

Install
pod 'GeoTracking', :git => 'https://bimal_mobile_dev@bitbucket.org/bimal_mobile_dev/geotracking.git', :tag => '1.0.0'

