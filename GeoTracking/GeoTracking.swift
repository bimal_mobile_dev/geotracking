//
//  GeoTracking.swift
//  GeoSparkLocation
//
//  Created by Senrysa Tech on 29/10/19.
//  Copyright © 2019 Bimal. All rights reserved.
//

import Foundation
import CoreLocation
import CoreMotion
import MapKit

public class GeoTracking : NSObject, CLLocationManagerDelegate,MKMapViewDelegate{

    let motionManager = CMMotionManager()
    
    let locationManager = CLLocationManager()
    
    public var mapView = MKMapView()
    
    let newPin = MKPointAnnotation()
    
    public func startUpdating() {
        
        locationManager.delegate = self
        mapView.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        motionManager.deviceMotionUpdateInterval = 1.0 / 60.0

        motionManager.startDeviceMotionUpdates(to: OperationQueue.main) {
            [weak self](data: CMDeviceMotion?, error: Error?) in

            guard let data = data else { return }

            let attitude: CMAttitude = data.attitude
            print(attitude)
        }
    }

    public func stopUpdating() {
        locationManager.stopUpdatingLocation()
        motionManager.stopDeviceMotionUpdates()
    }

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.first != nil {
            
              mapView.removeAnnotation(newPin)

              let location = locations.last! as CLLocation

              let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
              let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))

              //set region on the map
              mapView.setRegion(region, animated: true)

              newPin.coordinate = location.coordinate
              mapView.addAnnotation(newPin)
            
            print("latitude: ", location.coordinate.latitude)
            print("longitude: ", location.coordinate.longitude)
//            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//             let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//             self.mapView.setRegion(region, animated: true)
            
        }
    }
    
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

       guard !(annotation is MKUserLocation) else {
                let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "userLocation")
                annotationView.image = UIImage(named:"pin")
                return annotationView
            }
    return nil
    }
    
    
}
